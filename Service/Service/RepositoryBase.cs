﻿using System;
using System.Linq.Expressions;
using ServiceStack.Data;
using ServiceStack.OrmLite;

namespace Service
{
    /// <summary>
    /// Contains basic repository methods to call DB
    /// </summary>
    public class RepositoryBase
    {
        /// <summary>
        /// Contains factory to produce connection
        /// </summary>
        public IDbConnectionFactory DbConnectionFactory { set; private get; }

        /// <summary>
        /// Adds row to table 
        /// </summary>
        /// <typeparam name="T">Corresponds DB table</typeparam>
        /// <param name="instance">Corresponds row to add</param>
        /// <returns>Id if succeeded, else long.MinValue</returns>
        public long Add<T>(T instance)
        {
            try
            {
                using (var db = DbConnectionFactory.OpenDbConnection())
                {
                    db.Insert(instance);
                    return db.LastInsertId();
                }
            }
            catch (Exception)
            {
                return long.MinValue;
            }
        }

        /// <summary>
        /// Selects all from DB table
        /// </summary>
        /// <typeparam name="T">Corresponds DB table</typeparam>
        /// <returns>Array of type T</returns>
        public T[] GetInstances<T>()
        {
            using (var db = DbConnectionFactory.OpenDbConnection())
                return db.Select<T>().ToArray();
        }

        /// <summary>
        /// Selects from DB table by condition
        /// </summary>
        /// <typeparam name="T">Corresponds DB table</typeparam>
        /// <param name="predicate">Condition for select</param>
        /// <returns>Array of type T</returns>
        public T[] Select<T>(Expression<Func<T, bool>> predicate)
        {
            using (var db = DbConnectionFactory.OpenDbConnection())
                return db.Select<T>(predicate).ToArray();
        }

        /// <summary>
        /// Executes SQL cmd and returns array of T
        /// </summary>
        /// <typeparam name="T">List of this type is expected back</typeparam>
        /// <param name="sql">SQL cmd to execute</param>
        /// <param name="anonType">Parameters for SQL cmd</param>
        /// <returns>Array of T</returns>
        public T[] SqlList<T>(string sql, object anonType = null)
        {
            using (var db = DbConnectionFactory.OpenDbConnection())
                return db.SqlList<T>(sql, anonType).ToArray();
        }
    }
}