﻿using System;
using ServiceStack;
using ServiceStack.DataAnnotations;
using ServiceStack.FluentValidation;

namespace Service
{

    #region Common
    /// <summary>
    /// Represents SMS send result
    /// </summary>
    public enum EState { Success, Failed };

    /// <summary>
    /// Presents SMS table in DB
    /// </summary>
    public class Sms
    {
        [AutoIncrement]
        public long Id { set; get; }
        public DateTime DateTime { set; get; }
        [ForeignKey(typeof(Country),OnDelete = "CASCADE")]
        public long CountryId { get; set; }
        public string From { set; get; }
        public string To { set; get; }
        public decimal Price { get; set; }
        public EState State { set; get; }
    }
    #endregion

    #region /sms/send
    /// <summary>
    /// Send SMS request
    /// </summary>
    [Route("/sms/send", "GET")]
    public class SendSms
    {
        public string From { set; get; }
        public string To { set; get; }
        public string Text { set; get; }
    }

    /// <summary>
    /// Send SMS responce
    /// </summary>
    public class SendSmsResponce
    {
        public EState State { set; get; }
    }

    /// <summary>
    /// Send SMS parameters validation
    /// </summary>
    public class SendSmsValidator : AbstractValidator<SendSms>
    {
        public SendSmsValidator()
        {
            RuleFor(e => e.From).NotEmpty().Matches(@"^[a-zA-Z' '0-9]+$");
            RuleFor(e => e.To).NotEmpty().Matches(@"^\+\d{3,}$");
            RuleFor(e => e.Text).Length(1,160);
        }
    }
    #endregion

    #region /sms/sent
    /// <summary>
    /// Sent SMS request
    /// </summary>
    [Route("/sms/sent", "GET")]
    public class SentSms
    {
        public DateTime DateTimeFrom { set; get; }
        public DateTime DateTimeTo { set; get; }
        public int Skip { set; get; }
        public int Take { set; get; }
    }

    /// <summary>
    /// Sent SMS responce
    /// </summary>
    public class SentSmsResponce
    {
        public int TotalCount { set; get; }
        public Sms[] Sms { set; get; }
    }

    /// <summary>
    /// Sent SMS parameters validation
    /// </summary>
    public class SentSmsValidator : AbstractValidator<SentSms>
    {
        public SentSmsValidator()
        {
            RuleFor(e => e.DateTimeFrom).NotEmpty();
            RuleFor(e => e.DateTimeTo).NotEmpty();
            RuleFor(e => e.DateTimeFrom).LessThan(e => e.DateTimeTo);
            RuleFor(e => e.Skip).GreaterThanOrEqualTo(0);
            RuleFor(e => e.Take).GreaterThanOrEqualTo(1);
        }
    }
    #endregion
}