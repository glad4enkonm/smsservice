﻿namespace Service
{
    public class CountriesService : ServiceStack.Service
    {
        /// <summary>
        /// DB repository for Countries
        /// as far as only base functionality needed - base class used
        /// </summary>
        public RepositoryBase CountryRepository { get; set; }

        /// <summary>
        /// Entry point for GET /countries
        /// </summary>
        public object Get(GetCountries request)
        {
            return new GetCountriesResponce {Countries = CountryRepository.GetInstances<Country>() };
        }
    }
}