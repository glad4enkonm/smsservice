﻿using System;
using ServiceStack;
using ServiceStack.FluentValidation;

namespace Service
{
    /// <summary>
    /// Statistics record
    /// </summary>
    public class StatisticRecord
    {
        public DateTime Day { set; get; }
        public string Mcc { set; get; }
        public decimal PricePerSms { set; get; }
        public int Count { set; get; }
        public decimal TotalPrice { set; get; }
    }

    /// <summary>
    /// Request for GET /statistics
    /// </summary>
    [Route("/statistics","GET")]
    public class GetStatistics
    {
        public DateTime DateFrom { set; get; }
        public DateTime DateTo { set; get; }
        public string MccList { set; get; }
    }

    /// <summary>
    /// Responce for GET /statistics
    /// </summary>
    public class GetStatisticsResponce
    {
        public StatisticRecord[] StatisticRecords { set; get; }
    }

    /// <summary>
    /// Request validation for GET /statistics
    /// </summary>
    public class GetStatisticsValidator : AbstractValidator<GetStatistics>
    {
        public GetStatisticsValidator()
        {
            RuleFor(e => e.DateFrom).NotEmpty();
            RuleFor(e => e.DateTo).NotEmpty();
            RuleFor(e => e.DateFrom).LessThanOrEqualTo(e => e.DateTo);
            RuleFor(e => e.MccList).Matches(@"^(\d*|\d+(,\d+)+)$");
        }
    }
}