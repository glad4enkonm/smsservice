﻿using System;
using System.Web.Configuration;
using Funq;
using ServiceStack;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using ServiceStack.Validation;

namespace Service
{
    public class Global : System.Web.HttpApplication
    {

        public class SmsServiceAppHost : AppHostBase
        {
            public SmsServiceAppHost() : base("SMS Service", typeof(SmsService).Assembly)
            {
            }

            /// <summary>
            /// Drops all DB tables
            /// Method used to support unit tests
            /// </summary>
            public static void CleanDataBase(IDbConnectionFactory dbConnectionFactory)
            {
                const string spSql =
                    @"DROP procedure IF EXISTS `get_statistics_for_period_by_mcc`;";

                using (var db = dbConnectionFactory.OpenDbConnection())
                {
                    db.DropTable<Sms>();
                    db.DropTable<Country>();
                    db.ExecuteSql(spSql);
                }
            }

            public static Country[] SeedCountries =
            {
                new Country {Name = "Germany", Mcc = "262", Cc = "49", PricePerSms = 0.055M},
                new Country {Name = "Austria", Mcc = "232", Cc = "43", PricePerSms = 0.053M},
                new Country {Name = "Poland", Mcc = "260", Cc = "48", PricePerSms = 0.032M}
            };

            public static void SeedDataBase(IDbConnectionFactory dbConnectionFactory)
            {
                const string spSql =
                @"CREATE PROCEDURE `get_statistics_for_period_by_mcc`(IN  dateFrom datetime, IN dateTo datetime, IN mccCodes nvarchar(2000))
                    BEGIN
                        select
                            date(s.DateTime) as Day, sum(s.Price) as TotalPrice,
		                    c.Mcc, c.PricePerSms, count(*) as Count
                        from sms as s
                            inner join country as c on s.CountryId = c.id
                        where
                            date(s.DateTime) between dateFrom and dateTo
                            and FIND_IN_SET(c.Mcc, mccCodes)
	                    group by
                            Day, c.Mcc
                        order by
                            Day, c.Mcc;
                    END";

                using (var db = dbConnectionFactory.OpenDbConnection())
                {
                    if (!db.CreateTableIfNotExists<Country>()) return; // suppouse if this table exists DB is ready for use
                    db.Insert(SeedCountries);
                    db.CreateTable<Sms>();
                    db.ExecuteSql(spSql);
                }
            }

            /// <summary>
            /// General settings for service
            /// </summary>
            public override void Configure(Container container)
            {
                // Configure DB connection
                var dbConnectionFactory = 
                    new OrmLiteConnectionFactory(WebConfigurationManager.ConnectionStrings["mySql"].ConnectionString, MySqlDialect.Provider);
                SeedDataBase(dbConnectionFactory);
                container.Register<IDbConnectionFactory>(dbConnectionFactory);
                // Setup dummy SMS sender
                container.Register<ISmsSender>( new DummySmsSender());
                // Register dependency injections
                container.RegisterAutoWired<SmsRepository>();
                container.RegisterAutoWired<StatisticsRepository>();
                container.RegisterAutoWired<RepositoryBase>();
                //Manage plugins
                Plugins.Add(new ValidationFeature());
                container.RegisterValidators(typeof(SmsService).Assembly);
            }
        }

        protected void Application_Start(object sender, EventArgs e)
        {
            new SmsServiceAppHost().Init();
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {

        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}