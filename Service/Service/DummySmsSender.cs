﻿using System;
using System.IO;
using System.Web.Configuration;

namespace Service
{
    public class DummySmsSender : ISmsSender
    {
        /// <summary>
        /// Path for text file with sms
        /// </summary>
        public readonly string TxtResolvedFilePath;

        /// <summary>
        /// Constructor for dummy sms sender implementation
        /// saves sms to txt file
        /// </summary>
        /// <param name="txtFilePath">Path for text file with sms</param>
        public DummySmsSender(string txtFilePath = null)
        {
            txtFilePath = txtFilePath ?? WebConfigurationManager.AppSettings["DummuSmsSenderPath"];
            TxtResolvedFilePath = txtFilePath.Replace("%tmp%", Path.GetTempPath());
        }

        /// <summary>
        /// Saves SMS to txt file
        /// </summary>
        /// <param name="from">Sender</param>
        /// <param name="to">To number</param>
        /// <param name="text">SMS text</param>
        /// <returns>True if succeeded</returns>
        public bool SendSms(string from, string to, string text)
        {
            try
            {
                using (var w = File.AppendText(TxtResolvedFilePath))
                {
                    w.WriteLine(from);
                    w.WriteLine(to);
                    w.WriteLine(text);
                    w.WriteLine("-");
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }            
        }
    }
}