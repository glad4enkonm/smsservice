﻿namespace Service
{
    public class StatisticsService : ServiceStack.Service
    {
        /// <summary>
        /// DB operations for statistics
        /// </summary>
        public StatisticsRepository StatisticsRepository { get; set; }

        /// <summary>
        /// Entry point for GET /statistics
        /// </summary>
        public object Get(GetStatistics request)
        {
            return new GetStatisticsResponce
                { StatisticRecords = StatisticsRepository.GetStatisticRecords(request)};
        }
    }

    /// <summary>
    /// Extends RepositoryBase for statistics
    /// </summary>
    public class StatisticsRepository : RepositoryBase
    {
        public StatisticRecord[] GetStatisticRecords(GetStatistics request)
        {
            const string sqlToExec = "CALL get_statistics_for_period_by_mcc(@DateFrom, @DateTo, @MccList)";
            return SqlList<StatisticRecord>(sqlToExec, request);
        }
    }
}