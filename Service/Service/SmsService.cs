﻿using System;
using System.Linq;

namespace Service
{
    public class SmsService : ServiceStack.Service
    {
        /// <summary>
        /// Interface for SMS sender, allows to replace dummy implementation
        /// </summary>
        public ISmsSender SmsSender { get; set; }

        /// <summary>
        /// DB operations for SMS table
        /// </summary>
        public SmsRepository SmsRepository { get; set; }

        /// <summary>
        /// Entry point for GET /sms/send
        /// </summary>
        /// <param name="request">Request params</param>
        /// <returns>SendSmsResponce</returns>
        public object Get(SendSms request)
        {
            var res = SmsSender.SendSms(request.From, request.To, request.Text);
            var state = res ? EState.Success : EState.Failed;
            SmsRepository.AddSms(request, state);
            return new SendSmsResponce {State = state};
        }

        /// <summary>
        /// Entry point for GET /sms/sent
        /// </summary>
        /// <param name="request">Request params</param>
        /// <returns>SentSmsResponce</returns>
        public object Get(SentSms request)
        {
            var sentSms = SmsRepository.GetSentSms(request);
            return new SentSmsResponce {TotalCount = sentSms.Length, Sms = sentSms };
        }
    }

    /// <summary>
    /// Extends RepositoryBase for SMS DB table
    /// </summary>
    public class SmsRepository : RepositoryBase
    {
        /// <summary>
        /// Add SMS with link to Country
        /// </summary>
        public void AddSms(SendSms sendSmsRequest, EState state)
        {
            var countryCode = sendSmsRequest.To.Substring(1, 2);
            var countriesMatchingThisCode = Select<Country>(c => c.Cc == countryCode);

            if (countriesMatchingThisCode.Length != 1)
                throw new ArgumentException($"Country with code {countryCode} is not supported");
            var toCountry = countriesMatchingThisCode[0];

            var sms = new Sms
            {
                To = sendSmsRequest.To,
                From = sendSmsRequest.From,
                DateTime = DateTime.Now,
                CountryId = toCountry.Id,
                Price = toCountry.PricePerSms,
                State = state
            };
            Add(sms);
        }

        /// <summary>
        /// Select SMS in date period with skip & take
        /// </summary>
        public Sms[] GetSentSms(SentSms request)
        {
            var sms =  Select<Sms>(s => 
                (s.DateTime >= request.DateTimeFrom && s.DateTime <= request.DateTimeTo));

            return sms.Skip(request.Skip)
                .Take(request.Take).ToArray();
        }
    }
}