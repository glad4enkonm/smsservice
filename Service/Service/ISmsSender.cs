﻿namespace Service
{
    /// <summary>
    /// Sms sender interface alows to replace dummy realization
    /// </summary>
    public interface ISmsSender
    {
        /// <summary>
        /// Sends SMS
        /// </summary>
        /// <param name="from">From abonent</param>
        /// <param name="to">To number</param>
        /// <param name="text">SMS text</param>
        /// <returns>True on success</returns>
        bool SendSms(string from, string to, string text);
    }
}
