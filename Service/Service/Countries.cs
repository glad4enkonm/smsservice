﻿using ServiceStack;
using ServiceStack.DataAnnotations;

namespace Service
{
    /// <summary>
    /// GET /countries request
    /// </summary>
    [Route("/countries", "GET")]
    public class GetCountries
    {
    }

    /// <summary>
    /// GET /countries responce
    /// contains array of countries
    /// </summary>
    public class GetCountriesResponce
    {
        public Country[] Countries { set; get; }
    }

    /// <summary>
    /// Representation of DB Country table
    /// </summary>
    public class Country
    {
        [AutoIncrement]
        public long Id { get; set; }
        public string Name { get; set; }
        public string Mcc { get; set; }
        public string Cc { get; set; }
        public decimal PricePerSms { get; set; }

        public override bool Equals(object obj)
        {
            var country = obj as Country;
            return (country != null) ? Equals(country) : base.Equals(obj);
        }

        public bool Equals(Country c)
        {
            if (c == null) return false;
            return (Name == c.Name) && (Mcc == c.Mcc)
                   && (Cc == c.Cc) && (PricePerSms == c.PricePerSms);
        }
    }

}