﻿using System.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ServiceStack.OrmLite;

namespace Service.Tests
{
    [TestClass]
    public class GlobalTests
    {
        [TestMethod]
        public void SeedDataBase_crets_db_schema()
        {
            var dbConnectionFactory =
                    new OrmLiteConnectionFactory(ConfigurationManager.ConnectionStrings["mySql"].ConnectionString, MySqlDialect.Provider);
            
            Global.SmsServiceAppHost.CleanDataBase(dbConnectionFactory);
            Global.SmsServiceAppHost.SeedDataBase(dbConnectionFactory);
        }
    }
}