﻿using System;
using System.Configuration;
using System.Linq;
using Funq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ServiceStack;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using ServiceStack.Validation;

namespace Service.Tests
{
    [TestClass]
    public class StatisticsTest
    {
        private class SmsServiceAppHostStatTest : AppSelfHostBase
        {
            public SmsServiceAppHostStatTest() : base("SMS Service statistics test", typeof(SmsService).Assembly) { }

            public override void Configure(Container container)
            {

                var dbConnectionFactory =
                    new OrmLiteConnectionFactory(ConfigurationManager.ConnectionStrings["mySql"].ConnectionString,
                        MySqlDialect.Provider);

                Global.SmsServiceAppHost.CleanDataBase(dbConnectionFactory);
                Global.SmsServiceAppHost.SeedDataBase(dbConnectionFactory);
                container.Register<IDbConnectionFactory>(dbConnectionFactory);
                container.RegisterAutoWired<RepositoryBase>();
                container.RegisterAutoWired<SmsRepository>();
                container.RegisterAutoWired<StatisticsRepository>();
                container.Register<ISmsSender>(new DummySmsSender());

                //Manage plugins
                Plugins.Add(new ValidationFeature());
                container.RegisterValidators(typeof(SmsService).Assembly);
            }
        }

        private const string BaseUri = "http://localhost:2000/";

        [TestMethod]
        public void Check_GetStatistics_rest()
        {
            using (new SmsServiceAppHostStatTest().Init().Start(BaseUri))
            {
                var client = new JsonServiceClient(BaseUri);

                var seed = Global.SmsServiceAppHost.SeedCountries;

                var dtToday = DateTime.Now.Date;
                // send fiew messages
                var fSeed = seed.First();
                var respSend =
                    client.Get<SendSmsResponce>(new SendSms {From = "Me", To = $"+{fSeed.Cc}1", Text = "Text"});
                Assert.AreEqual(EState.Success, respSend.State);
                respSend =
                    client.Get<SendSmsResponce>(new SendSms {From = "Me", To = $"+{fSeed.Cc}1", Text = "Text"});
                Assert.AreEqual(EState.Success, respSend.State);
                // check stat
                var stat =
                    client.Get<GetStatisticsResponce>(new GetStatistics
                    {
                        DateFrom = dtToday,
                        DateTo = dtToday,
                        MccList = fSeed.Mcc
                    });
                var statRow = stat.StatisticRecords.First();
                Assert.AreEqual(dtToday, statRow.Day);
                Assert.AreEqual(fSeed.Mcc, statRow.Mcc);
                Assert.AreEqual(fSeed.PricePerSms*2, statRow.TotalPrice);
                Assert.AreEqual(2, statRow.Count);
            }
        }
    }
}
