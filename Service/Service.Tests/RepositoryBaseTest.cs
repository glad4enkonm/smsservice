﻿using System;
using System.Configuration;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ServiceStack.OrmLite;

namespace Service.Tests
{
    [TestClass]
    public class RepositoryBaseTest
    {
        private readonly RepositoryBase _rBase = new RepositoryBase();

        public RepositoryBaseTest()
        {
            _rBase.DbConnectionFactory =
                new OrmLiteConnectionFactory(ConfigurationManager.ConnectionStrings["mySql"].ConnectionString,
                    MySqlDialect.Provider);
        }

        [TestMethod]
        public void RepositoryBase_select()
        {
            var someCountry = Global.SmsServiceAppHost.SeedCountries.First();
            var all = _rBase.Select<Country>(c => c.Mcc == someCountry.Mcc);
            Assert.AreEqual(all.Length, 1);
        }

        [TestMethod]
        public void RepositoryBase_add()
        {
            var someRandomString = Guid.NewGuid().ToString();
            var all = _rBase.Select<Sms>(s => s.From == someRandomString);
            Assert.AreEqual(all.Length, 0);
            var id = _rBase.Add(new Sms {From = someRandomString, CountryId = 1});
            all = _rBase.Select<Sms>(s => s.Id == id);
            Assert.AreEqual(all.First().From, someRandomString);
        }

        [TestMethod]
        public void RepositoryBase_getInstances()
        {
            var all = _rBase.GetInstances<Country>();
            Assert.AreEqual(all.Length, Global.SmsServiceAppHost.SeedCountries.Length);
            foreach (var seedCountry in Global.SmsServiceAppHost.SeedCountries)
            {
                var countryFormDb = all.First(c => c.Mcc == seedCountry.Mcc);
                seedCountry.Id = countryFormDb.Id;
                Assert.AreEqual( seedCountry, countryFormDb);
            }
        }

        [TestMethod]
        public void RepositoryBase_sqlList()
        {
            var all = _rBase.SqlList<Country>("select * from country;");
            Assert.AreEqual(all.Length, Global.SmsServiceAppHost.SeedCountries.Length);
        }
    }
}
