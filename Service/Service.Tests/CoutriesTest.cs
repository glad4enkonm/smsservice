﻿using System.Configuration;
using System.Linq;
using Funq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ServiceStack;
using ServiceStack.Data;
using ServiceStack.OrmLite;

namespace Service.Tests
{
    /// <summary>
    /// Checks /countries
    /// </summary>
    [TestClass]
    public class CoutriesTest
    {

        private class SmsServiceAppHostCountriesTest : AppSelfHostBase
        {
            public SmsServiceAppHostCountriesTest() : base("SMS Service countries test", typeof(SmsService).Assembly) { }

            public override void Configure(Container container)
            {
                var dbConnectionFactory =
                    new OrmLiteConnectionFactory(ConfigurationManager.ConnectionStrings["mySql"].ConnectionString,
                        MySqlDialect.Provider);

                Global.SmsServiceAppHost.SeedDataBase(dbConnectionFactory);
                container.Register<IDbConnectionFactory>(dbConnectionFactory);
                container.RegisterAutoWired<RepositoryBase>();
            }
        }

        private const string BaseUri = "http://localhost:2000/";

        [TestMethod]
        public void Check_countries_rest()
        {

            using (new SmsServiceAppHostCountriesTest().Init().Start(BaseUri))
            {
                var client = new JsonServiceClient(BaseUri);

                //GET /customers
                var all = client.Get<GetCountriesResponce>(new GetCountries());
                Assert.AreEqual(all.Countries.Length, Global.SmsServiceAppHost.SeedCountries.Length);
                foreach (var country in Global.SmsServiceAppHost.SeedCountries)
                    Assert.AreEqual(country, all.Countries.First(c => c.Mcc == country.Mcc));

            }

        }
    }
}
