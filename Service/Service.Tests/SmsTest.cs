﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using Funq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ServiceStack;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using ServiceStack.Validation;

namespace Service.Tests
{
    [TestClass]
    public class SmsTest
    {
        private class SmsServiceAppHostSmsTest : AppSelfHostBase
        {
            public SmsServiceAppHostSmsTest() : base("SMS Service SMS test", typeof(SmsService).Assembly)
            {
            }

            public override void Configure(Container container)
            {

                var dbConnectionFactory =
                    new OrmLiteConnectionFactory(ConfigurationManager.ConnectionStrings["mySql"].ConnectionString,
                        MySqlDialect.Provider);
                Global.SmsServiceAppHost.CleanDataBase(dbConnectionFactory);
                Global.SmsServiceAppHost.SeedDataBase(dbConnectionFactory);
                container.Register<IDbConnectionFactory>(dbConnectionFactory);
                container.RegisterAutoWired<RepositoryBase>();
                container.RegisterAutoWired<SmsRepository>();
                container.Register<ISmsSender>(new DummySmsSender());

                //Manage plugins
                Plugins.Add(new ValidationFeature());
                container.RegisterValidators(typeof(SmsService).Assembly);
            }
        }

        private const string BaseUri = "http://localhost:2000/";
        private ServiceStackHost _appHost;
        private JsonServiceClient _client;

        [TestInitialize]
        public void InitTests()
        {

            Trace.WriteLine("init");
            _appHost = new SmsServiceAppHostSmsTest().Init().Start(BaseUri);
            _client = new JsonServiceClient(BaseUri);
        }

        [TestCleanup]
        public void CleanTests()
        {

            Trace.WriteLine("clean");
            _appHost.Dispose();
        }

        private static void CheckEx(Action exRunner, Type exType, string expectedMsg, string failMsg)
        {
            try
            {
                exRunner();
                Assert.Fail(failMsg);
            }
            catch (Exception e)
            {
                Assert.AreEqual(exType, e.GetType());
                Assert.AreEqual(expectedMsg, e.Message);
            }
        }

        #region Validation SendSms tests

        #region SendSms

        [TestMethod]
        public void SendSms_validation_from_not_empty()
        {

            var request = new SendSms {From = null, To = "+491", Text = "Text"};
            CheckEx(() => _client.Get<SendSmsResponce>(request), typeof(WebServiceException), "NotEmpty",
                "Expected WebServiceException exception");
        }

        [TestMethod]
        public void SendSms_validation_from_match()
        {
            var request = new SendSms {From = ":", To = "+491", Text = "Text"};
            CheckEx(() => _client.Get<SendSmsResponce>(request), typeof(WebServiceException), "RegularExpression",
                "Expected WebServiceException exception");
        }

        [TestMethod]
        public void SendSms_validation_to_not_empty()
        {
            var request = new SendSms {From = "Me", To = null, Text = "Text"};
            CheckEx(() => _client.Get<SendSmsResponce>(request), typeof(WebServiceException), "NotEmpty",
                "Expected WebServiceException exception");
        }

        [TestMethod]
        public void SendSms_validation_to_match()
        {
            var request = new SendSms {From = "Me", To = "+4", Text = "Text"};
            CheckEx(() => _client.Get<SendSmsResponce>(request), typeof(WebServiceException), "RegularExpression",
                "Expected WebServiceException exception");
        }

        [TestMethod]
        public void SendSms_validation_text_length()
        {
            var request = new SendSms {From = "Me", To = "+491", Text = null};
            CheckEx(() => _client.Get<SendSmsResponce>(request), typeof(WebServiceException), "Length",
                "Expected WebServiceException exception");
            request.Text = "";
            CheckEx(() => _client.Get<SendSmsResponce>(request), typeof(WebServiceException), "Length",
                "Expected WebServiceException exception");
            request.Text = new string('?', 161);
            CheckEx(() => _client.Get<SendSmsResponce>(request), typeof(WebServiceException), "Length",
                "Expected WebServiceException exception");
        }

        #endregion

        #region SentSms

        [TestMethod]
        public void SentSms_validation_dtfrom_less_than_dtto()
        {

            var request = new SentSms
            {
                DateTimeFrom = DateTime.Now.AddHours(1),
                DateTimeTo = DateTime.Now,
                Skip = 0,
                Take = 1
            };
            CheckEx(() => _client.Get<SentSmsResponce>(request), typeof(WebServiceException), "LessThan",
                "Expected WebServiceException exception");
        }

        [TestMethod]
        public void SentSms_validation_skip_greather_or_eq_0()
        {

            var request = new SentSms
            {
                DateTimeFrom = DateTime.Now,
                DateTimeTo = DateTime.Now.AddHours(1),
                Skip = -1,
                Take = 1
            };
            CheckEx(() => _client.Get<SentSmsResponce>(request), typeof(WebServiceException), "GreaterThanOrEqual",
                "Expected WebServiceException exception");
        }

        [TestMethod]
        public void SentSms_validation_take_greather_or_eq_1()
        {

            var request = new SentSms
            {
                DateTimeFrom = DateTime.Now,
                DateTimeTo = DateTime.Now.AddHours(1),
                Skip = 0,
                Take = 0
            };
            CheckEx(() => _client.Get<SentSmsResponce>(request), typeof(WebServiceException), "GreaterThanOrEqual",
                "Expected WebServiceException exception");
        }

        #endregion

        #endregion

        [TestMethod]
        public void Check_SendSms_SentSms_rest()
        {
            var dtFrom = DateTime.Now.AddHours(-1);
            var dtTo = DateTime.Now.AddHours(1);
            const string toNum = "+497";
            //GET /sms/send?from=Me&to=%2B497&text=Text
            Trace.WriteLine("_client.Get<SendSmsResponce>");
            var respSend = _client.Get<SendSmsResponce>(new SendSms {From = "Me", To = toNum, Text = "Text"});
            Assert.AreEqual(EState.Success, respSend.State);
            //GET /sms/sent?dateTimeFrom=2016-08-01T11:30:20&dateTimeTo=2016-12-01T09:20:22&skip=0&take=10
            SentSmsResponce respSent;

            Trace.WriteLine("_client.Get<SentSmsResponce>");
            respSent =
                _client.Get<SentSmsResponce>(new SentSms
                {
                    DateTimeFrom = dtFrom,
                    DateTimeTo = dtTo,
                    Skip = 0,
                    Take = 1
                });
            Trace.WriteLine(respSent.Sms.Length);
            var sms = respSent.Sms.First(s => s.To == toNum);
            Assert.IsNotNull(sms);
            Assert.AreEqual(respSent.TotalCount, 1);
        }
    }
}

