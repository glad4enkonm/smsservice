﻿using System.Configuration;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Service.Tests
{
    [TestClass]
    public class DummySmsSenderTest
    {
        [TestMethod]
        public void It_can_create_txt_file_with_content()
        {
            var dummySmsSender = new DummySmsSender(ConfigurationManager.AppSettings["DummuSmsSenderPath"]);

            var path = dummySmsSender.TxtResolvedFilePath;
            if (File.Exists(path)) File.Delete(path);

            var sms = new {From = "Me", To = "911", Text = "Need aditional weekends, on this was coding."};
            var res = dummySmsSender.SendSms(sms.From, sms.To, sms.Text);
            Assert.IsTrue(res);
            Assert.IsTrue(File.Exists(path));
        }
    }
}
